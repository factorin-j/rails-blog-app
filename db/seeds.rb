# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

## create admin user
users = User.create([
  {username: 'admin', 'password': 'admin'},
  {username: 'testuser', 'password': 'testuser'},
])

post_title = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit'
post_contents = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent suscipit risus sem, vel semper mauris commodo non.
Fusce lobortis dignissim condimentum. Proin venenatis lectus sit amet nisi imperdiet, in auctor libero commodo. 
Nulla ultricies porttitor neque. In hac habitasse platea dictumst. Interdum et malesuada fames ac ante ipsum primis in faucibus. 
Vivamus tempor pulvinar nunc, id varius erat bibendum sit amet. Nullam mollis dapibus neque. 
Curabitur tincidunt est dolor, nec imperdiet turpis suscipit id.

Fusce eget lacus et ex tempor malesuada. Aliquam ac tempus ligula. Quisque id erat non orci congue ultricies. 
Vestibulum eget pulvinar ligula. Suspendisse quis erat felis. Donec ipsum elit, molestie in nibh lobortis, viverra maximus augue. 
Maecenas lobortis, orci et tincidunt elementum, erat tortor fermentum nulla, volutpat maximus turpis metus posuere quam.

Curabitur varius, nisi gravida tempor commodo, nisl leo blandit felis, vel porta sem tortor luctus diam. 
Nam at dolor faucibus, cursus mi vel, maximus ipsum. Fusce a commodo arcu. In faucibus consectetur mi a facilisis. 
Proin ullamcorper est ut nisl cursus efficitur convallis quis velit. Nulla velit eros, fermentum ac urna vel, aliquam eleifend diam. 
Etiam vitae lorem vel enim convallis mattis.'

## create posts
post_data = []
(0..9).each do |i|
  post_data.push({
    title: post_title,
    contents: post_contents,
    path: "test-post-#{i}",
    user_id: users[(i % 2)].id
  })
end

posts = Post.create(post_data)

comment_data = []
posts.each do |post|
  (0..rand(0..5)).each do |i|
    user = users[(i % 2)]
    comment_data.push({
      post_id: post.id,
      user_id: user.id,
      message: "message comment here uid: #{user.id}, pid: #{post.id}"
    })
  end
end

Comment.create(comment_data)

## Pre-requisite
```bash
ruby --version
gem --version

## run `gem install bundler` if not installed
bundle --version 
```

## Installation
```bash
## cd to clone repository
cd <repo>

## install gem dependencies
bundle install

## create .env file
cp .env.dist .env

## change database settings
vim .env 

## create database
bin/rails db:create

## migrate tables
bin/rails db:migrate

## optional: creates mock data
bin/rails db:seed 

## start rails server
bin/rails server
```

Then connect to [http://127.0.0.1:3000]()

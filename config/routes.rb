Rails.application.routes.draw do
  get 'users/register', to: 'users#register'
  get 'users/logout', to: 'users#logout'
  get 'users/login', to: 'users#login'
  get 'posts/:path', to: 'posts#view'

  post 'users/register', to: 'users#register_exec'
  post 'users/login', to: 'users#login_exec'
  post 'posts/:path/comment', to: 'posts#comment_exec'

  root to: 'home#index'
end

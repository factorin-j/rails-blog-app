class UsersController < ApplicationController
  def get_next_path
    request[:next].nil? ? request.env['HTTP_REFERER'] : request[:next]
  end

  def logout
    session.delete(:user_id)
    session.destroy

    redirect_to get_next_path
  end

  def login
    session[:redirect_to] = get_next_path
  end

  def login_exec
    @user = User.find_by(username: request[:user][:username])
    if !@user.nil? && @user.authenticate(request[:user][:password])
      session[:user_id] = @user.id
      redirect_to_referer
      return
    end

    flash[:error] = 'invalid user login'
    redirect_to_login
  end

  def register
    session[:redirect_to] = get_next_path
  end

  def register_exec
    @user = User.new(request[:user])
    if @user.save
      session[:user_id] = @user.id
      redirect_to '/'
    end
  end
end

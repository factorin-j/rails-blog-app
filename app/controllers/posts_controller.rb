class PostsController < ApplicationController
  before_action :fetch_user
  def view
    @post = Post.find_by_path(request[:path])
    if @post.nil?
      raise ActiveRecord::RecordNotFound
    end
  end

  def comment_exec
    require_user
    @post = Post.find_by_path(request[:path])

    comment = Comment.new
    comment.user = @user
    comment.post = @post
    comment.message = request[:message]
    comment.save

    redirect_to "/posts/#{request[:path]}"
  end
end

class HomeController < ApplicationController
  before_action :fetch_user
  def index
    @posts = Post.all.paginate(:page => params[:page], :per_page => 5)
  end
end

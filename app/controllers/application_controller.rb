class ApplicationController < ActionController::Base
  rescue_from ActiveRecord::RecordNotFound, :with => :render_not_found

  def render_not_found(exception)
    puts exception
    render :template => 'errors/404', :status => 404
  end

  def require_user
    @user = fetch_user
    if @user.nil?
      redirect_to_login
      return
    end

    session[:user_id] = @user.id
  end

  def fetch_user
    user_id = session[:user_id]
    @user = user_id.nil? ? nil : User.find(user_id)
  end

  def redirect_to_login
    session[:redirect_to] = request.referer
    redirect_to '/users/login'
  end

  def redirect_to_referer
    referer = session[:redirect_to]
    referer = (referer.nil?) ? '/' : referer

    session.delete(:redirect_to)
    redirect_to referer
  end
end
